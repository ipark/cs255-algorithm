import os
import string
import sys

"""
LZW Compression
---------------
Initialize code table
Always start by sending a clear code to the code stream.
Read first index from index stream. This value is now the value for the index buffer
<LOOP POINT>
    Get the next index from the index stream to the index buffer. 
    We will call this index, K
    Is index buffer + K in our code table?
        Yes:
            add K to the end of the index buffer
            if there are more indexes, return to LOOP POINT
    No:
        Add a row for index buffer + K into our code table with the next smallest code
        Output the code for just the index buffer to our code steam
        Index buffer is set to K
        K is set to nothing
    if there are more indexes, return to LOOP POINT
Output code for contents of index buffer
Output end-of-information code
"""


def LZW_compression(charStream, roots, verbose):
    # init dictionary (k,v) = (string,code)
    dict = {k: i + 1 for i, k in enumerate(roots)}

    if verbose:
        # show the creation of table, should remove if test for large files        
        #####################
        hints = ['prefix', 'curChar', 'curStr', 'isCurStrInDict', 'addedDictItem',
                 'output']
        print(hints[0].ljust(10), hints[1].ljust(10), hints[2].ljust(9),
              hints[3].ljust(10), hints[4].ljust(15), hints[5].ljust(15))
        ###################

    char = string = output = ''
    isCurStrInDict = False

    lastCode = len(dict.keys())
    prefix = ''
    codeStream = []
    # read one character at a time from input char-stream
    for char in charStream:
        string = str(prefix + char) # form a string
        if verbose:
            # remove if test for large files                                  
            ###################
            print(prefix.ljust(10), char.ljust(10), string.ljust(10), end="")
            ###################

        # string is found in dictionary
        if string in dict:  
            output = ''
            isCurStrInDict = True
            prefix = string # update prefix
        else: # string is not in dictionary
            output = str(dict[prefix])
            isCurStrInDict = False
            prefix = char # update prefix
            dict[string] = lastCode + 1 # add to dictionary
            lastCode += 1

        if verbose:
            # remove if test for large files                           
            ###################
            print(str(isCurStrInDict).ljust(15), end="")
            if isCurStrInDict:
                print('')
            else:
                print(str(string + ':' + str(dict[string])).ljust(15),
                      output.ljust(15))
            ###################

        if output != '':
            codeStream.append(output)

    output = str(dict[prefix])

    if verbose:
        # remove if test for large files                            
        ###################
        print(prefix.ljust(10), 'empty'.ljust(10), prefix.ljust(9),
              'True'.ljust(14), ''.ljust(15), output.ljust(15))
        ###################

    codeStream.append(output)  # output code-stream
    return codeStream, dict


"""
LZW Decompression
-----------------
Initialize code table
let CODE be the first code in the code stream
output {CODE} to index stream
<LOOP POINT>
    let CODE be the next code in the code stream
    is CODE in the code table?
    Yes:
        output {CODE} to index stream
        let K be the first index in {CODE}
        add {CODE-1}+K to the code table
    No:
        let K be the first index of {CODE-1}
        output {CODE-1}+K to index stream
        add {CODE-1}+K to code table
return to LOOP POINT
"""


def LZW_decompression(codeStream, roots, verbose):
    # init dictionary (k,v) = (code,string)
    dictionary = {i + 1: k for i, k in enumerate(roots)}
    lastCode = len(dictionary.keys())

    if verbose:
        # show the creation of table, should remove if test for large files 
        ###################
        hints = ['curCode', 'oldCodeStr', 'isCurCodeInDict', 'curCodeStr',
                 'addedDictItem']
        print(hints[0].ljust(15), hints[1].ljust(15), hints[2].ljust(15),
              hints[3].ljust(15), hints[4].ljust(15))
        ###################

    isCurCodeInDict = True
    string = oldCodeStr = ''
    charStream = []

    # read one code at a time from code-stream
    for cur_code in codeStream:
        cur_code = int(cur_code)
        # if current code is found in the dictionary
        if cur_code in dictionary:
            string = dictionary[cur_code] # update a string based on current code
            isCurCodeInDict = True
        else: # if current code is not in the dictionary
            string = oldCodeStr + oldCodeStr[0]  # update a string based on old code
            isCurCodeInDict = False

        if oldCodeStr != '':
            lastCode += 1
            dictionary[lastCode] = oldCodeStr + string[0] # add to dictionary

        if verbose:
            #########                                               
            # remove if test for large files
            print(str(cur_code).ljust(15), oldCodeStr.ljust(15),
                  str(isCurCodeInDict).ljust(15), string.ljust(15),
                  str(lastCode) + ':' + dictionary[lastCode])
            #######

        oldCodeStr = string
        charStream.append(string)

    return charStream, dictionary

# merge multiple text files 
def MergePerFolder(path, output):
    files = os.listdir(path)
    with open(output, 'w') as outfile:
        for filename in files:
            if len(filename) < 4:
                with open(os.path.join(path, filename)) as infile:
                    for line in infile:
                        outfile.write(line)
                infile.close()
    outfile.close()
    return output

def readFile(file):
    lines = []
    with open(file, 'r', encoding='UTF-8') as input_file:
        for line in input_file:
            line = ''.join([c if c in string.printable else '' for c in line])
            lines.append(line)
    input_file.close()
    charStream = ''.join(lines)
    return charStream

#############
# Test Case 1
#############
"""
verbose = True # False if larger text
roots = ['/', 'a', 'b', 'o', 'w']
givenCharStream = "wabba/wabba/wabba/wabba/woo/woo/woo"
correctCodeStream = "#5#2#3#3#2#1#6#8#10#12#9#11#7#16#5#4#4#11#21#23#4"
"""


#############
# Test Case 2
#############
verbose = True # False if larger text
roots = ['a','b','c']
givenCharStream = "ababcbababaaaaaa"
correctCodeStream = "#1#2#4#3#5#8#1#10#11"


#############
# Test Case 3 (file)
#############
"""
verbose = False # False if larger text
roots = [i for i in string.printable]
#file = 'TextFiles/spider.txt'
file = 'TextFiles/yesterday.txt'
givenCharStream = readFile(file)
#print(givenCharStream)
"""


#############
# Test Case 4 (big file)
#############
"""
verbose = False # True if simple text
roots = [i for i in string.printable]
mergedfile = MergePerFolder("TextFiles/corpus.dos", 'text')
givenCharStream = readFile(mergedfile)
#print(givenCharStream)
"""

#############
# Test Case 5 (big file)
#############
"""
verbose = False # True if simple text
roots = [i for i in string.printable]
givenCharStream = ""
with open("TextFiles/dickens.txt", encoding="latin-1") as f:
    for n, line in enumerate(f):
        line = line.strip()
        line = line.encode("ascii", "ignore").decode()
        if len(line) > 0:
            givenCharStream += line
"""


###################
# compression 
###################
# input: charStream
# output: codeStream
# dictionary (k,v) = (string, code)
print("===============")
print("LZW Compression")
print("===============")


if verbose:
    print(f"INPUT: charStream={givenCharStream}")
else:
    print(f"INPUT: charStreamLen={len(givenCharStream)}")
print(f"       roots={roots}")
print()

if verbose:
    print("ADAPTIVE DICTIONARY BUILD DURING COMPRESSION:")

codeStream, dictionary = LZW_compression(givenCharStream, roots, verbose)
                         ###############
print()

if verbose:
    print(f"OUTPUT: codeStream={'#'+'#'.join([str(c) for c in codeStream])}")
    print()

#   print("VERIFICATION:", correctCodeStream == '#'+'#'.join([str(c) for c in codeStream])) 
#   print()


print(f"SIZE REDUCTION    : inputLen({len(givenCharStream)}) -> outputLen({len(codeStream)})")
print(f"COMPRESSION RATIO : {len(codeStream)*100/len(givenCharStream):3.2f}")
print()

if verbose:
    print("CODE TABLE")
    print(" string : code")
    for k,v in dictionary.items():
        print(f' {k:4s} : {v:4d}')
    print()


###################
# decompression
###################
# input: codeStream
# output: charStream
# dictionary (k,v) = (code, string)
print("=================")
print("LZW Decompression")
print("=================")

if verbose:
    print(f"INPUT: codeStream={'#'+'#'.join([str(c) for c in codeStream])}")
else:
    print(f"INPUT: codeStreamLen={len(codeStream)}")
print(f"       roots={roots}")
print()

if verbose:
    print("ADAPTIVE DICTIONARY BUILD DURING DECOMPRESSION:")

charStream, dictionary = LZW_decompression(codeStream, roots, verbose)
                         ##################
print()

if verbose:
    print(f"OUTPUT: charStream={''.join(charStream)}")
    print()

print("VERIFICATION: ", givenCharStream == ''.join(charStream))
print("               [originalText == recoveredText (compression->decompression)]")
print()

if verbose:
    print("CODE TABLE")
    print(" code : string")
    for k,v in dictionary.items():
        print(f'{k:5d} : {v:5s}')
