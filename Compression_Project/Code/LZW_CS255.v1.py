import sys, string
"""
LZW Compression
---------------
Initialize code table
Always start by sending a clear code to the code stream.
Read first index from index stream. This value is now the value for the index buffer
<LOOP POINT>
    Get the next index from the index stream to the index buffer. 
    We will call this index, K
    Is index buffer + K in our code table?
        Yes:
            add K to the end of the index buffer
            if there are more indexes, return to LOOP POINT
        No:                                                                                
            Add a row for index buffer + K into our code table with the next smallest code
            Output the code for just the index buffer to our code steam
            Index buffer is set to K
            K is set to nothing
        if there are more indexes, return to LOOP POINT
Output code for contents of index buffer
Output end-of-information code
"""
def LZW_compression(charStream, roots):
    # init dictionary (k,v) = (string,code)
    dictionary = {k:i+1 for i, k in enumerate(roots)}
    lastCode = len(dictionary.keys())
    #
    prefix = ""
    codeStream = []
    for char in charStream:
        string = prefix + char
        if string in dictionary:
            prefix = string
        else:
            if prefix in dictionary:
                codeStream.append(dictionary[prefix]) 
            dictionary[string] = lastCode + 1
            lastCode += 1
            prefix = char
    if prefix in dictionary:
            codeStream.append(dictionary[prefix])
    return codeStream, dictionary
        
"""
LZW Decompression
-----------------
Initialize code table
let CODE be the first code in the code stream
output {CODE} to index stream
<LOOP POINT>
    let CODE be the next code in the code stream
    is CODE in the code table?
    Yes:
        output {CODE} to index stream
        let K be the first index in {CODE}
        #add {CODE-1}+K to the code table
    No:
        let K be the first index of {CODE-1}
        output {CODE-1}+K to index stream
        ##add {CODE-1}+K to code table
return to LOOP POINT
"""
def LZW_decompression(codeStream, roots):
    # init dictionary (k,v) = (code,string)
    dictionary = {i+1:k for i, k in enumerate(roots)}
    lastCode = len(dictionary.keys()) + 1
    #
    charStream = []
    cur_code = codeStream.pop(0)
    charStream.append(dictionary[cur_code])
    old_code = cur_code

    for cur_code in codeStream:
        if cur_code in dictionary:
            # output {CODE} to index stream
            charStream.append(dictionary[cur_code]) 
            # let K be the first index in {CODE}
            K = dictionary[cur_code][0]
            # add {CODE-1}+K to the code table
            dictionary[lastCode] = dictionary[old_code] + K
            old_code = cur_code
        else:
            # let K be the first index of {CODE-1}
            K = dictionary[old_code][0]
            # output {CODE-1}+K to index stream
            charStream.append(dictionary[old_code] + K)
            # add {CODE-1}+K to code table
            dictionary[lastCode] = dictionary[old_code] + K
            old_code = cur_code
        lastCode += 1


    return charStream, dictionary

#############
# Test Case 1
#############
"""
roots = ['/','a','b','o','w']
givenCharStream = "wabba/wabba/wabba/wabba/woo/woo/woo"
correctCodeStream ="#5#2#3#3#2#1#6#8#10#12#9#11#7#16#5#4#4#11#21#23#4" 
"""
#############
# Test Case 2
#############
"""
roots = ['a','b','c']
givenCharStream = "ababcbababaaaaaa"
correctCodeStream = "#1#2#4#3#5#8#1#10#11"
"""
##########################
# Test Case 3 = large text including ascii and non-ascii characters
##########################
import string
asciiSet = string.ascii_letters +  string.digits +  string.punctuation + ' '
roots = [str(c) for c in asciiSet]
givenCharStream = ""
with open("TextFiles/dickens.txt", encoding="latin-1") as f:
    for n, line in enumerate(f):
        line = line.strip()
        line = line.encode("ascii", "ignore").decode()
        if len(line) > 0:
            givenCharStream += line

##########################
# Test Case 4 = genomic sequence 
##########################


###################
# compression 
###################
# input: charStream
# output: codeStream
# dictionary (k,v) = (string, code)
print("===============")
print("LZW Compression")
print("===============")
print(f"INPUT: charStreamLen={len(givenCharStream)}")
#print(f"INPUT: charStream={givenCharStream}")
print(f"       roots={roots}")
print()
codeStream, dictionary = LZW_compression(givenCharStream, roots)
#print(f"OUTPUT: codeStream={'#'+'#'.join([str(c) for c in codeStream])}")
print()
#print("VERIFICATION:", correctCodeStream == '#'+'#'.join([str(c) for c in codeStream])) 
print(f"SIZE REDUCTION : inputLen({len(givenCharStream)}) -> outputLen({len(codeStream)})")
print(f"RATIO %        : {len(codeStream)*100/len(givenCharStream):3.2f}")
print()
#print("CODE TABLE")
#print(" string : code")
#for k,v in dictionary.items():
#    print(f' {k:4s} : {v:4d}')
#print()


###################
# decompression
###################
# input: codeStream
# output: charStream
# dictionary (k,v) = (code, string)
print("=================")
print("LZW Decompression")
print("=================")
print(f"INPUT: codeStreamLen={len(codeStream)}")
#print(f"INPUT: codeStream={'#'+'#'.join([str(c) for c in codeStream])}")
print(f"       roots={roots}")
print()
charStream, dictionary = LZW_decompression(codeStream, roots)
print(f"OUTPUT: charStream={''.join(charStream)}")
print(f"GIVEN: {givenCharStream}")
#print()
print("VERIFICATION: ", givenCharStream == ''.join(charStream))
print()
#print("CODE TABLE")
#print(" code : string")
#for k,v in dictionary.items():
#    print(f'{k:5d} : {v:5s}')
