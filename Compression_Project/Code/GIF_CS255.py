import sys, math
# this LZW_compression for GIF-compression is 
# almost same as regular LZW_compression
# STEP1: char-stream -> code-stream
def LZW_compression(charStream, roots):
    # init dictionary (k,v) = (string,code)
    dictionary = {k:i for i, k in enumerate(roots)}
    lastCode = len(dictionary.keys())
    #
    prefix = ""
    codeStream = []
    # GIF specific extra info: minCodeSize 
    minCodeSize = int(math.log2(len(roots))) 
    for char in charStream:
        string = prefix + char
        if string in dictionary:
            prefix = string
        else:
            # GIF specific special code <CC> and <EOI>
            if lastCode == 2 ** minCodeSize:
                dictionary['<CC>'] = lastCode 
                dictionary['<EOI>'] = lastCode + 1
                lastCode = lastCode + 1
                codeStream.append(dictionary['<CC>'])
            codeStream.append(dictionary[prefix])
            dictionary[string] = lastCode + 1
            lastCode += 1
            prefix = char
    if prefix:
        codeStream.append(dictionary[prefix])
    # GIF-specific output: terminator
    codeStream.append(dictionary['<EOI>'])
    return codeStream, dictionary
        
# This is special function for GIF-compression
# STEP2: code-stream -> bit-stream 
# STEP3: bit-stream -> hexadecimal (which is stored in GIF file)
def codeStreamTohexString(codeStream, minCodeSize):
    codeBits = minCodeSize
    index = 0
    bits = []
    printBits = []
    # code-stream -> bit-stream
    for code in codeStream:
        # every 2**codeBits encountered, reset index and increase code-bit by 1
        if index == 2 ** codeBits:
            index = 0
            codeBits += 1
        bits.insert(0, (f"{code:0{codeBits + 1}b}"))
        printBits.insert(0, (f"{code:0{codeBits + 1}b}|")) # bebug-purpose
        index += 1
    binStr  = ''.join(bits)
    print(''.join(printBits))

    # bit-stream -> hexadecimal (which is stored in GIF file)
    hexStr = []
    while len(binStr) > 0:
        byteRead = binStr[-8:]
        binStr = binStr[:len(binStr)-8]
        hexStr.append(hex(int(byteRead, 2)).replace('0x','').upper().zfill(2))
    hexStr.insert(0, hex(len(hexStr)).replace('0x','').upper().zfill(2))
    hexStr.insert(0, hex(minCodeSize).replace('0x','').upper().zfill(2))
    hexStr.append('00')
    return ' '.join(hexStr)

#############
# Test Case (10x10 GIF image)
#############
roots = ['0','1','2','3']
givenCharStream="1111122222111112222211111222221110000222111000022222200001112220000111222221111122222111112222211111"
correctCodeStream="#4#1#6#6#2#9#9#7#8#10#2#12#1#14#15#6#0#21#0#10#7#22#23#18#26#7#10#29#13#24#12#18#16#36#12#5"
###################
# compression 
###################
# input: charStream
# output: codeStream
# dictionary (k,v) = (string, code)
print("===============")
print("LZW COMPRESSION")
print("===============")
print(f"INPUT:\n charStream={givenCharStream}")
print(f" roots = {roots} # 1=white, 2=red, 3=blue, 4=black\n")
codeStream, dictionary = LZW_compression(givenCharStream, roots)
print(f"OUTPUT:\n codeStream={'#'+'#'.join([str(c) for c in codeStream])}\n")
print(f"SIZE REDUCTION:\n inputLen({len(givenCharStream)}) -> outputLen({len(codeStream)})\n")
print(f"COMPRESSION RATIO : {len(codeStream)*100/len(givenCharStream):3.2f}")
print()
print("CODE TABLE")
print(" string       code ")
for k in dictionary.keys():
    print(f' {k:7s} : {dictionary[k]:7d}')
print()
#Verification
print("VERIFICATION:", correctCodeStream == '#'+'#'.join([str(c) for c in codeStream])) 
print()

###################
# GIF compression 
###################
# input: codeStream
# output: ImageData in Hex Format
minCodeSize = 2
correctImageData="02 16 8C 2D 99 87 2A 1C DC 33 A0 02 75 EC 95 FA A8 DE 60 8C 04 91 4C 01 00"
print("===============")
print("GIF COMPRESSION")
print("===============")
print(f"INPUT:\n codeStream={'#'+'#'.join([str(c) for c in codeStream])}\n")
print()
print("ADAPTIVE BINARY BIT SIZE OUTPUT :")
resultImageData = codeStreamTohexString(codeStream, minCodeSize)
print()
print(f"OUTPUT:\n ImageData=hex({resultImageData})\n")
print(f"SIZE REDUCITON:\n inputBytes({sys.getsizeof(givenCharStream)}) -> outputBytes({sys.getsizeof(resultImageData.replace(' ',''))})\n")
print(f"COMPRESSION RATIO : {sys.getsizeof(resultImageData)*100/sys.getsizeof(givenCharStream):3.2f}")
print()
print("VERIFICATION:", correctImageData == resultImageData)
print()

