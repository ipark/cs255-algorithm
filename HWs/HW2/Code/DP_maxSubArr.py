def maxSubArry(P):
    n = len(P)
    # memoization
    dp = [0] * n
    for end in range(n):
        if end == 0:
            dp[end] = P[end]
        dp[end] = max(dp[end - 1] + P[end], P[end])
    print(end, dp)
    return max(dp) 

# Test
P = [-9, 10, -8, 10, 5, -4, -2, 5]
print(maxSubArry(P))
