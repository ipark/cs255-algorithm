def CashierAlgo(n, coin_denom): ######### O(k^n)
    S = [] # coin set S
    k = 0 # index of coin denominator
    coin_denom.sort(reverse=True)  # sort coins large to small 25,10,5,1
    while n > 0: # total amount
        ck = coin_denom[k] # greedy-choice on coin denominator
        if not ck: return  # if no ck <= n, return 
        (ck_num, n) = divmod(n, ck) # divmod computes (quotient, remainder)
        while ck_num > 0: 
            S.append(ck); ck_num -= 1 # append to coin set S
        k += 1 # next index of coin denominator
    return S, sum(S), len(S)
# Tst
coin_denom = [1, 5, 10, 25]
#coin_denom = [1, 10, 25]
n = 30
(setS, sumS, lenS) = CashierAlgo(n, coin_denom)
print("Cashier's Algorithm by Greedy-choice")
print(f"coin_denom={coin_denom}; n = {n}")
print(f"S = {setS} | sum(S) = {sumS} | num_coins = {lenS}")

