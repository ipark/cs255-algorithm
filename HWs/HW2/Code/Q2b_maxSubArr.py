import numpy as np
start_end = []
dp = []
def maxSubArr(P):
    # recursive function
    ##################
    def subSumMax(start, end):
        # base case
        if start == end:
            return P[start]
        mid = (start + end) // 2
        return max(subSumMax(start, mid),\  ######## recursive call
                   subSumMax(mid + 1, end),\    ######## recursive call
                   midCrossingMax(start, mid, end)) ######## recursive call
    ##################
    def midCrossingMax(start, mid, end):
        # left
        leftMax, leftSum = float("-inf"), 0
        for l in range(mid, start - 1, -1): # downto start
            leftSum += P[l]
            if leftSum > leftMax:
                leftMax = leftSum
                start_end = [_ for _ in range(l, mid+1)]
        # right
        rightMax, rightSum = float("-inf"), 0
        for r in range(mid + 1, end + 1): # upto end
            rightSum += P[r]
            if rightSum > rightMax:
                rightMax = rightSum
                Rstart_end = (mid+1, r)
                start_end.extend([_ for _ in range(mid+1, r+1)])
                dp.append(start_end)
        return leftMax + rightMax # including P[mid]
    # main ##################################
    n = len(P)
    return subSumMax(0, n - 1)
    #########################################
# Test
P = [-9, 10, -8, 10, 5, -4, -2, 5]
maxSubArr(P)
print(f"maxSum = {maxSubArr(P)} = sum( P[{dp[-1][0]}..{dp[-1][-1]}] )")
#
