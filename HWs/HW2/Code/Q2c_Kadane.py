def Kadane(P):
    n = len(P)
    maxSum, maxTemp = 0, 0
    depart, arrive = 0, 0
    for i in range(n):##################### This main loop O(N)-time
        maxTemp += P[i]
        if maxTemp < 0: # negative
            maxTemp = 0 
            depart = i # reset depart
        if maxTemp > maxSum:
            maxSum = maxTemp
            arrive = i # update arrive
    if maxSum < 0:
        depart, arrive = -1, -1
    return (maxSum, depart, arrive)

# Test
P = [-9, 10, -8, 10, 5, -4, -2, 5]
(maxSum, depart, arrive) = Kadane(P)
print(f"maxSum={maxSum}, departIndex={depart}, arriveIndex={arrive}")
