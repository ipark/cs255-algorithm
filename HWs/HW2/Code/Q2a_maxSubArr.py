dp = []
def maxSubArry(P):
    n = len(P)
    maxPay = 0
    start_end = (0, 0)      ############ Overall O(n^2)-time
    for start in range(n):  ############ outer for-loop : O(n)-time
        subSum = 0
        for end in range(start, n): ########### nested inner for-loop: O(n)-time
            subSum += P[end]
            if subSum > maxPay:
                maxPay = subSum
                start_end = (start, end)
                dp.append(start_end)
    return maxPay

# Test
P = [-9, 10, -8, 10, 5, -4, -2, 5]
print(f"maxSum = {maxSubArry(P)} = sum( P[{dp[-1][0]}..{dp[-1][1]}] )")
