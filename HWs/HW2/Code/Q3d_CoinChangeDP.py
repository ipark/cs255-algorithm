def CoinChangeDP(n, coin_denom): ########## O(k*n) 
    minCoinsDP = [float('inf')] * (n + 1) # memoization to record min num of coins
    minCoinsDP[0] = 0
    for ck in coin_denom:  ################ O(k)
        for x in range(ck, n + 1): ######## O(n)
            minCoinsDP[x] = min(minCoinsDP[x], minCoinsDP[x-ck]+1)
    return minCoinsDP[n] if minCoinsDP[n] != float('inf') else -1
            
# Test
coin_denom = [1, 5, 10, 25]
#coin_denom = [1, 10, 25]
n = 30
print("Coin Change by DP")
print(f"coin_denom={coin_denom}; n = {n}")
print(f"number of coins = {CoinChangeDP(n, coin_denom)}")

