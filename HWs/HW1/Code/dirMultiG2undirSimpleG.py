"""
CS255 - HW1 - Inhee Park
Conversion from directed multi-graph to 
undirected simple-graph at O(V+E) time
"""
def dirMultiG2undirSimpleG(G):
    for v in G:             # O(V) for all vertices
        for u in G[v]:      # O(E) for adjacency list for all edges
            if v == G[u][0]: 
                G[u].remove(v) # O(1)
    return G

# Test
G = { 1 : [2,3,4],
      2 : [3],
      3 : [2],
      4 : [4],
      5 : [2]
    }
print("Directed Multi-Graph = ")
print(G)
print("Undirected Simple Graph =")
print(dirMultiG2undirSimpleG(G))
