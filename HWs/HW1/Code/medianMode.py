"""
CS255 - HW1 - Inhee Park
Time O(N); Space O(N)
"""
LatinChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

def medianMode(A):

    # L: length of 26
    # L is thus an array to save occurrence frequency 
    # of a latin letter # at the corresponding index of an array 
    # L[0] = 1 if there is a 'A' in an input array 
    # L[0] = 2 if there are two 'A's in an input array
    # L[25] = 1 if there is a 'Z' in an input array
    # where ord('A') = 65, 
    # thus an index of L is order(LatinLetter)-65
    L = [0 for i in range(len(LatinChars))]

    for i, a in enumerate(A):
        index = ord(a) - 65 # convert letter to number; ord('A')=65
        L[index] += 1  # increase by 1 at that char location

    # To find a mode, find the INDEX whose value is max in L
    # Then convert that INDEX to corresponding CHAR
    freqINDEX = L.index(max(L))
    mode = chr(freqINDEX + 65) # note ord('A')=65

    halfSize = (len(A) + 1) // 2
    traceSum = 0
    for i, l in enumerate(L): # one sweep through array L size of 26
        if l > 0:
            traceSum += l
            if traceSum >= halfSize: # traceSum is half size of input A
                midINDEX = i # save that INDEX 
                median = chr(midINDEX + 65)  # median is corresponding CHAR 
                print(f"Mode = {mode}, Median = {median}")
                return # terminate


# Test
#IN:  A = [F, A, D, A, B, A] 
#OUT: Mode = A, Median = A 
A = ['F', 'A', 'D', 'A', 'B', 'A'] 
#AAABDF
print(A)
medianMode(A)
print()
#
#INT: A = [G, F, R, R, C] 
#OUT: Mode = R, Median = G
A = ['G', 'F', 'R', 'R', 'C'] 
#CFGRR
print(A)
medianMode(A)

