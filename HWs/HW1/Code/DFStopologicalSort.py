"""
CS255 - HW1 - Inhee Park
Topological Sorting
Base Algorithm: DFS
"""
# Topological Sort                      # DFS
def DFS_topologySort(G):                #def DFS(G):                                     
    def DFS_visit(G, v, col):           #  def DFS_visit(G, v, col):
                                        #      visited.append(v)  immediate print
                                        #      ##################################
        col[v] = "gray"                 #      col[v] = "gray"
        for adj_v in G[v]:              #      for adj_v in G[v]:
            if col[adj_v] is "white":   #          if col[adj_v] is "white":
                DFS_visit(G, adj_v, col)#              DFS_visit(G, adj_v, col)
        # all done for vertex v         #      # all done for vertex v
        col[v] = "black"                #      col[v] = "black"
        finished.append(v)#v done,print #                                 
        #####MODIFICATION(1)#############
    finished = []                       #  visited = []
    col = {v: "white" for v in G}       #  col = {v: "white" for v in G}  
    for v in G:                         #  for v in G:                    
        if col[v] is "white":           #      if col[v] is "white":      
            DFS_visit(G, v, col)        #          DFS_visit(G, v, col)   
    return finished[::-1]               #  return visited
    # reverse order as it's a stack     #
    ##########MODIFICATION(2)############
# Test
G = {'a': [],
     'b': ['a', 'd'],
     'c': ['a'],
     'd': ['a', 'c'],
     'e': ['b', 'd', 'g'],
     'f': ['c', 'd'],
     'g': ['d', 'f'] 
     }
print("Topological Sorting Order = ", DFS_topologySort(G))
