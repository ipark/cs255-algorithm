"""
CS255 - HW1 - Inhee Park
Finding a position of zero 
from the two almost identical sorted arrays at O(lgN)-time
"""
# Array A is derived by inserting a  to array B.
# Thus we can assume that A < B.
#
# Since A and B is almost similar,
# just sweeping an entire array of B has O(N) time.
#
# Thus, should find better algorithm than O(N),
# possible candidate method is binary search to compute
# its middle index, then search either left half or right 
# half of the given array to find 0.
def whereIsZero(A, B):
    low, high = 0, len(B)
    while low <= high:
        mid = low + (high - low) // 2                           
        if B[mid] == 0:
            return mid + 1 # index starting from 1 not 0
        elif A[mid] == B[mid]: # indicating 0 is at right half      
                low = mid + 1
        elif A[mid] > B[mid]: # indicating 0 is at left half
                high = mid - 1
        else: # A[mid < B[mid] that can't happen as long as A < B
            print("A should be subarray of B")
            return -1
    return -1

# Test
A = [1, 3, 4, 6, 7, 8, 9, 20]
B = [1, 3, 0, 4, 6, 7, 8, 9, 20]
print(A)
print(B)
print("zero is at ", whereIsZero(A, B))
