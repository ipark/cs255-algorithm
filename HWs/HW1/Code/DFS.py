"""
CS255 - HW1 - Inhee Park
Depth-First-Search (DFS) using stack and 3 state (W,G,B)
"""
def DFS(G):
    def DFS_visit(G, v, col):
        visited.append(v) # immediate print
        col[v] = "gray"
        for adj_v in G[v]:
            if col[adj_v] is "white":
                DFS_visit(G, adj_v, col)
        # all done for vertex v
        col[v] = "black"

    visited = []
    col = {v: "white" for v in G}
    for v in G:
        if col[v] is "white":
            DFS_visit(G, v, col)
    return visited


# Test
G = {0 : [2, 3],
     1 : [2, 5, 6], 
     2 : [0, 1, 4], 
     3 : [0, 7], 
     4 : [2, 6], 
     5 : [1, 6], 
     6 : [1, 4, 5], 
     7 : [3]}
print("DFS order: ", DFS(G))
