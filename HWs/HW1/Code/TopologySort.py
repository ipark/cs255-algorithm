# from vertices and edges, build two Graphs 
def buildGraphs(Vs, Es):
    Gout = dict()# Gout (key, values) = (from_vertex, to_vertex)
    Gin = dict() # Gin  (key, values) = (to_vertex, from_vertex)
    noIns = []   # noIns = [vertex list without incoming]
    for v in Vs:
        Gout[v] = set() # set keys
        Gin[v] = set()  # set keys
    noIns = set(Vs)     # all vertices
    for f, t in Es:     # (fromV, keyV) from edge list
        Gout[f].add(t)  
        Gin[t].add(f)
        noIns.discard(t) # remain Vs not keys in Gin
    return Gout, Gin, noIns

# remove vertex and update graphs and noIns list
def removeV(v, Gout, Gin, noIns):
    for u in Gout[v]:   # vertex v's adjacent list 
        Gin[u].discard(v) # remove them from Gin graph
        if len(Gin[u]) == 0: # no incoming 
            noIns.add(u)     # add to noIns list
    if not Gout[v]: # no key vertex v 
        return
    del Gout[v] # delete key vertex v

# by removing vertex w/o incoming 
def ToplogySort(Gout, Gin, noIns, sortedVs):
    if len(noIns) == 0: # no more vertex w/o incoming vertex
        return          # stop
    v = noIns.pop()     # from the vertex w/o incoming 
    removeV(v, Gout, Gin, noIns) # remove it
    sortedVs.append(v) # toplogy sorting order
    return ToplogySort(Gout, Gin, noIns, sortedVs) # recursion w/ updated Graphs

# Test
Vs = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
Es = [ #from->to
       ('b', 'a'), 
       ('b', 'd'),
       ('c', 'a'),
       ('d', 'a'),
       ('d', 'c'),
       ('e', 'b'),
       ('e', 'd'),
       ('e', 'g'),
       ('f', 'c'),
       ('f', 'd'),
       ('g', 'd'),
       ('g', 'f')]
# build Graphs from edges and vertices
Gout, Gin, noIns = buildGraphs(Vs, Es)
sortedVs = []
# run topological sorting
ToplogySort(Gout, Gin, noIns, sortedVs)

if len(sortedVs) == len(Vs):
    print(f"DAC! Thus Toplogical Sorting ", sortedVs)
else:
    print("No DAC! No Toplogical Sorting")
