"""
CS255 - HW1 - Inhee Park
Lombardi Graph Drawing: 
finding number of shortest paths from s->e
Base algorithm: Shortest Path Algorithm based on the BSF
"""
def BFSshortestPath(G, start, end):
    def enqueue(q, v):
        q.append(v)
    def dequeue(q):
        return q.pop(0) 

    length = 0
    spLen = -1 # shortest path length
    spCount = 0 # count of shorted paths
    q = [(start, length)]
    visited = []
    print(f" (v={start}, len={length})")
    while q:
        (v, length) = dequeue(q)
        visited.append(v)
        if v == end:
            spLen = length
            spCount = 1
            print(f" (v={v}, len={length}) : FOUND shortest path!")
            ############################# MODIFCATION of BFS
            while q: # explore remaining vertices from queue
                (u, du) = dequeue(q)
                if du > spLen: # stop if longer than shortest path
                    print(f" (v={v}, len={du}) : Longer than shortest path")
                    break
                if u == v: # increase shortest path count if u is end vertex
                    spCount += 1
                    print(f" (v={v}, len={du}) : FOUND shortest path!")
                else:
                    print(f" (v={v}, len={du})")
            break # CRUCIAL stop point
            ############################# MODIFCATION of BFS
        for adjv in G[v]:
            if adjv not in visited:
                print(f" (v={v}, len={length})")
                enqueue(q, (adjv, length + 1))
    return spCount

G = {0 : [2, 3],
     1 : [2, 5, 6], 
     2 : [0, 1, 4],
     3 : [0, 7], 
     4 : [2, 6], 
     5 : [1, 6], 
     6 : [1, 4, 5],
     7 : [3]
     }
G[2].append(8)
G[6].append(8)
G[8] = [2, 6]
start, end = 2, 6
print("Lombardi:")
print(f"Number of shortest path {start}->{end} = ", BFSshortestPath(G, start, end))
