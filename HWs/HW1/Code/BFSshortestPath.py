"""
CS255 - HW1 - Inhee Park
Finding Shortest Path Length from s->e
Base algorithm: BSF using queue
"""
def BFSshortestPath(G, start, end):
    def enqueue(q, v):
        q.append(v)
    def dequeue(q):
        return q.pop(0) 

    visited = []
    length = 0 
    spLen = -1 # shortest path length
    q = [(start, length)]
    print(f"START: (v={start}, len={length})")
    while q:
        (v, length) = dequeue(q)
        visited.append(v) # keep tracking of visit or not
        if v == end:
            spLen = length
            print(f"END  : (v={v}, len={spLen})")
            return spLen
        for u in G[v]:
            if u not in visited:
                enqueue(q, (u, length + 1))
                visited.append(u)
    return spLen # when no shortest path, return -1
# Test
G = {0 : [2, 3],
     1 : [2, 5, 6], 
     2 : [0, 1, 4], 
     3 : [0, 7], 
     4 : [2, 6], 
     5 : [1, 6], 
     6 : [1, 4, 5], 
     7 : [3]}
start = 0
end = 5
print(f"Length of shortest path {start}->{end} = {BFSshortestPath(G, start, end)}")
