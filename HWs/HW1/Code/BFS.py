"""
CS255 - HW1 - Inhee Park
Breadth-First-Search (BFS) using queue
"""
def BFS(G, v):
    def enqueue(q, v):
        q.append(v)
    def dequeue(q):
        return q.pop(0) 

    q, visited = [], []
    visited.append(v)
    enqueue(q, v)
    while q:
        w = dequeue(q)
        for adj in G[w]:
            if adj not in visited:
                visited.append(adj)
                enqueue(q, adj)
    return visited

G = {0 : [2, 3],
     1 : [2, 5, 6], 
     2 : [0, 1, 4], 
     3 : [0, 7], 
     4 : [2, 6], 
     5 : [1, 6], 
     6 : [1, 4, 5], 
     7 : [3]}
print("BFS order: ", BFS(G, 0))
