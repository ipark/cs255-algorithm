CS255 : Design and Analysis of Algorithms
=========
<ul>
<li>Stable Matching (Gale-Shapley Algorithm)</li>
<li>Running time, growth of functions</li>
<li>Graphs, BFS, DFS, topological sorting</li>
<li>Number-Theoretic Algorithms, Searching</li>
<li>Greedy Algorithms: Scheduling, Shortest paths, Caching, Knapsack</li>
<li>Greedy Algorithms: Minimum spanning tree, clustering</li>
<li>Divide & Conquer: sorting, integer/matrix multiplication, max subarray</li>
<li>Divide & Conquer: computational geometry</li>
<li>Dynamic Programming: scheduling, knapsack</li>
<li>Dynamic Programming: all pair shortest path</li>
<li>Network flow, applications</li>
<li>Heaps, Amortized Analysis </li>
<li>Amortized Analysis cont.</li>
<li>Randomization: Quicksort, Hashing</li>
<li>Intractability, P, NP, NP-completeness, reductions, time hierarchy</li>
<li>Approximation Algorithms: vertex cover, TSP, path coloring</li>
</ui>

CS255 Project : Data Compression using Dictionary-based Technique Lempel–Ziv–Welch (LZW) and GIF Variant Image Data Compression
=========

Abstract
--------
<blockquote>
We focus on the ``lossless'' compression algorithms using a dictionary.
Dictionary data structure is efficient in case of heavy look-up operations, generally has linear time at O(N) 
albeit of pre-computation is required. In the LZW algorithm, we could greedily utilize dictionary data structure even further by adaptively generating dictionary 
by taking advantage of the frequency redundancy of character-stream.Furthermore, GIF compression is a variant of LZW compression with extra compression by taking advantage of extra code-size information to use small size bits for earlier code, adaptively incrementing a code-size for later code on top of the already efficient LZW compression scheme.
We observed that as the length of the input character-stream becomes longer, the more compression is achieved because of frequently appeared string patterns are mapped onto indexes, thus more compact representation is possible.
As the LZW is relatively straightforward to implement, thus not only be useful to larger text file compression, but also can be applied to many other types of data such as binary files (roots=0,1),  hexadecimal strings (roots=1,2,3,4,5,6,7,8,9,A,B,C,D,E,F), genomic sequence compression (roots='A','T','C','G') as we've explored with GIF compression.
</blockquote>

### Codes: [Python src code](https://gitlab.com/ipark/cs255-algorithm/-/tree/master/Compression_Project/Code)

### Report: [Report](https://gitlab.com/ipark/cs255-algorithm/-/blob/master/Compression_Project/Report/CS255-InheePark-XinxinYang-Final.pdf)

### Presentation: [Slide](https://gitlab.com/ipark/cs255-algorithm/-/blob/master/Compression_Project/Presentation/CS255-LZW-presentation-InheeXinxin.pdf)